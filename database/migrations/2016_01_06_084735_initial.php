<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class Initial extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{

		if(Schema::hasTable('USERS')){Schema::drop('USERS');}
		if(Schema::hasTable('APPLICATIONS')){Schema::drop('APPLICATIONS');}
		if(Schema::hasTable('DOCUMENTS')){Schema::drop('DOCUMENTS');}
		if(Schema::hasTable('PAYMENTS')){Schema::drop('PAYMENTS');}
		if(Schema::hasTable('COMMENTS')){Schema::drop('COMMENTS');}
		if(Schema::hasTable('APPLICATIONS_DOCUMENTS')){Schema::drop('APPLICATIONS_DOCUMENTS');}
		if(Schema::hasTable('APPLICATIONS_COMMENTS')){Schema::drop('APPLICATIONS_COMMENTS');}
		if(Schema::hasTable('USERS_APPLICATIONS')){Schema::drop('USERS_APPLICATIONS');}
		if(Schema::hasTable('PAYMENTS_DOCUMENTS')){Schema::drop('PAYMENTS_DOCUMENTS');}
		if(Schema::hasTable('password_resets')){Schema::drop('password_resets');}

		Schema::create('USERS', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('email')->unique();
			$table->string('password', 60);
			$table->enum('role',['User','Admin','Registrar','Cashier','Clerk']);
			$table->rememberToken();
			$table->timestamps();
		});

		Schema::create('APPLICATIONS',function(Blueprint $table){
			$table->increments('id');
			$table->string('claimant');
			$table->string('defendant');
			$table->string('suitno');
			$table->string('judge');
			$table->enum('status',['Awaiting assessment','Awaiting payment','Awaiting Payment Verification'
                ,'Assigning Case','Awaiting Trial',
				'Awaiting Judgement','Completed']);
			$table->timestamps();
		});

		Schema::create('DOCUMENTS', function(Blueprint $table){
			$table->increments('id');
			$table->string('name');
			$table->string('document');
			$table->timestamps();
		});

		Schema::create('PAYMENTS', function(Blueprint $table){
			$table->increments('id');

			$table->string('amount');

			$table->enum('status',['Pending','Paid']);

			$table->integer('applications_id',false,true);
			$table->integer('users_id',false,true);

			$table->foreign('applications_id')->references('id')->on('APPLICATIONS')->onUpdate('cascade')
				->onDelete('no action');
			$table->foreign('users_id')->references('id')->on('USERS')->onUpdate('cascade')
				->onDelete('no action');

			$table->timestamps();
		});

		Schema::create('COMMENTS', function(Blueprint $table){
			$table->increments('id');
			$table->string('comment',5000);
			$table->timestamps();
		});

		Schema::create('APPLICATIONS_DOCUMENTS', function(Blueprint $table){
			$table->integer('applications_id',false,true);
			$table->integer('documents_id',false,true);

			$table->foreign('applications_id')->references('id')->on('APPLICATIONS')->onUpdate('cascade')
				->onDelete('no action');
			$table->foreign('documents_id')->references('id')->on('DOCUMENTS')->onUpdate('cascade')
				->onDelete('no action');
		});

		Schema::create('APPLICATIONS_COMMENTS', function(Blueprint $table){
			$table->integer('applications_id',false,true);
			$table->integer('comments_id',false,true);

			$table->foreign('applications_id')->references('id')->on('APPLICATIONS')->onUpdate('cascade')
				->onDelete('no action');
			$table->foreign('comments_id')->references('id')->on('COMMENTS')->onUpdate('cascade')
				->onDelete('no action');
		});


		Schema::create('USERS_APPLICATIONS', function(Blueprint $table){
			$table->integer('applications_id',false,true);
			$table->integer('users_id',false,true);

			$table->foreign('applications_id')->references('id')->on('APPLICATIONS')->onUpdate('cascade')
				->onDelete('no action');
			$table->foreign('users_id')->references('id')->on('USERS')->onUpdate('cascade')
				->onDelete('no action');
		});

		Schema::create('PAYMENTS_DOCUMENTS', function(Blueprint $table){
			$table->integer('payments_id',false,true);
            $table->integer('documents_id',false,true);

			$table->foreign('payments_id')->references('id')->on('PAYMENTS')->onUpdate('cascade')
				->onDelete('no action');
			$table->foreign('documents_id')->references('id')->on('DOCUMENTS')->onUpdate('cascade')
				->onDelete('no action');
		});

		Schema::create('password_resets', function(Blueprint $table)
		{
			$table->string('email')->index();
			$table->string('token')->index();
			$table->timestamp('created_at');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
