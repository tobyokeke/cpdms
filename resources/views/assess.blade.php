@extends('app')


@section('content')

<div class="container">

    @if(isset($list) && $list != "[]" )
    <h3>Unassessed Applications</h3>
    <table class="table table-responsive">
        <tr>
            <th>Claimant</th>
            <th>Defendant</th>
            <th>Date Applied</th>
            <th></th>
        </tr>

        @foreach($list as $item)
            <tr>
            <td>{{$item->claimant}}</td>
            <td>{{$item->defendant}}</td>
            <td>{{$item->created_at}}</td>
            <td> <a href="{{url('/appDetails/'.$item->id)}}"> <button style="width:80px" class="btn btn-primary">View</button> </a> </td>
            </tr>
        @endforeach

    </table>
    @endif
<br>
        @if( isset($attention) && $attention != "[]")
            <h3>Applications that need your attention</h3>
            <table class="table table-responsive">
                <tr>
                    <th>Claimant</th>
                    <th>Defendant</th>
                    <th>Amount</th>
                    <th></th>
                </tr>

                @foreach($attention as $item)
                    <tr>
                        <td>{{$item->claimant}}</td>
                        <td>{{$item->defendant}}</td>
                        <td>{{$item->amount}}</td>
                        <td> <a href="{{url('/appDetails/'.$item->id)}}"> <button style="width:80px;" class="btn btn-primary">View</button> </a> </td>
                    </tr>
                @endforeach

            </table>

        @endif
    @if(isset($assessed) && $assessed != "[]")
    <h3>Assessed Applications</h3>
    <table class="table table-responsive">
        <tr>
            <th>Claimant</th>
            <th>Defendant</th>
            <th>Amount</th>
            <th></th>
        </tr>

        @foreach($assessed as $item)
            <tr>
                <td>{{$item->claimant}}</td>
                <td>{{$item->defendant}}</td>
                <td>{{$item->amount}}</td>
                <td> <a href="{{url('/appDetails/'.$item->id)}}"> <button style="width:80px;" class="btn btn-primary">Change</button> </a> </td>
            </tr>
        @endforeach

    </table>
        @endif



</div>

@endsection