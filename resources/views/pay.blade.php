@extends('app')

@section('content')

    <div class="container">

    @if(isset($status))
        <p class="panel panel-success">{{$status}}</p>
    @endif

        @if(isset($application))
        Claimant: {{$application->claimant}} <br>
        Defendant: {{$application->defendant}} <br>
        Amount: {{$pay->amount}} <br>
        Status: {{$application->status }}

        <h3>Add payment verification documents</h3>
        <form method="post" enctype="multipart/form-data" action="{{url('/pay')}}" id="payForm">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="id" value="{{$pay->id}}">

        </form>

        <button onclick="add()" id="addDocument" class="btn btn-secondary">Add Document</button>
        <button onclick="done()" id="done" class="btn btn-secondary">Done</button>

        @endif

    </div>

    <script>
        function add(){
            var form = $('#payForm').append("<input type ='file' class='file form-control'>");
            var num = 1;
            $('.file').each(function(){
                this.setAttribute('name','file'+ num);
                num++;
            });
        }

        function done(){
            var number = 0;
            $('.file').each(function (){ number++; });
            var form = $('#payForm');
            form.append("<input type='hidden' name='number' value='" + number + "' >");
            form.append("<button id='submit' type='submit' style='display:none' class='btn btn-primary'>Add</button>")
            $('#submit').show();
            $('#addDocument').hide();
            $('#done').hide();
        }
    </script>

@endsection