@extends('app')

@section('content')

    <div class="container">
        <div class="row">
        @if(isset($status))
        <p>{{$status}}</p>
        @endif

        <h3>User Details</h3>

            <p>
             Claimant: {{$list->claimant}} <br>
             Defendant: {{$list->defendant}}<br>
             @if(isset($payment))
                 Amount: {{$payment->amount}} <br>
                    Payment Status: {{$payment->status}} <br>

             @endif

             @if($list->suitno != '')
                 Suit No: {{$list->suitno}} <br>
                 Judge: {{$list->judge}}
             @endif
            </p>

            <h3>Attached Documents</h3>
            <ul class="list-group">
        @foreach($attachments as $item)
           <li class="list-group-item col-md-3"> <a href="{{$item->document}}">{{$item->name}}</a> </li>
        @endforeach
            </ul>
        </div> <!-- end row div -->

        @if(!isset($payment))
            <form method="post" enctype="multipart/form-data" action="{{url('/appDetails')}}" class="form-group col-md-5">
                <input type="hidden" name="id" value="{{$list->id}}">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <label class="control-label">Amount:</label> <input type="text" name="amount">
                <button type="submit" class="btn btn-success">Send</button>
            </form>
        @endif

        @if(isset($payment) && $payment->amount != null && $payment->status == 'Pending')
                <form method="post" enctype="multipart/form-data" action="{{url('/appDetails/update')}}" class="form-group col-md-5">
                    <input type="hidden" name="id" value="{{$list->id}}">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <label class="control-label">Amount:</label> <input type="text" name="amount">
                    <button type="submit" class="btn btn-success">Change Amount</button>
                </form>
        @endif

                {{-- For Suit and judge assignment--}}
        <div class="row col-md-6"> <br>
        @if( isset($payment) && $payment->status == 'Paid' && $list->suitno == '')
            <form method="post" action ="{{url('/appDetails/assign/suit')}}" class="form-group col-md-5">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="id" value="{{$list->id}}">

                <label class="control-label">Suit/motion no</label>
                <input type="text" class="form-control" name="suitMotion" >

                <label class="control-label">Presiding Judge</label>
                <input type="text" class="form-control" name="judge"> <br>

                <button type="submit" class="btn btn-success">Assign</button>
            </form>
        @endif
        </div> <!-- end row div -->

        {{-- For comments --}}
        <div class="row col-md-6"> <br>
            @if( isset($payment) && $payment->status == 'Paid')
                <form method="post" action ="{{url('')}}" class="form-group col-md-5">
                    <input type="hidden" name="_token" value="{{csrf_token()}}">
                    <label class="control-label">Topic</label>
                    <input type="text" class="form-control" name="suitMotion" >

                    <label class="control-label">Comment </label>
                    <textarea  class="form-control" name="judge"> </textarea> <br>

                    <button type="submit" class="btn btn-success">Add</button>
                </form>
            @endif
        </div> <!-- end row div -->


    </div> <!-- end container div -->
@endsection