@extends('app')

@section('content')
    <div class="container">
        @if(isset($list))
            <h3>Your unassessed applications</h3>
            <table class="table table-responsive">
                <tr>
                    <th>Claimant</th>
                    <th>Defendant</th>
                    <th>Status</th>
                </tr>


                @foreach($list as $item)

                    <tr>
                        <td> {{$item->claimant}} </td>
                        <td> {{$item->defendant}} </td>
                        <td> {{$item->status}} </td>
                    </tr>
                @endforeach

            </table>

    @endif

            @if(isset($assessed))
                <h3>Your assessed applications</h3>
                <table class="table table-responsive">
                    <tr>
                        <th>Claimant</th>
                        <th>Defendant</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th></th>
                    </tr>


                    @foreach($assessed as $item)

                        <tr>
                            <td> {{$item->claimant}} </td>
                            <td> {{$item->defendant}} </td>
                            <td> {{$item->amount}}</td>
                            <td> {{$item->status}} </td>
                            <td>
                                @if($item->status != 'Awaiting Payment Verification')
                                <a href="{{url('/pay/'.$item->id)}}"><button class="btn btn-primary">Pay</button></a>
                                @endif
                            </td>
                        </tr>
                    @endforeach

                </table>

            @endif

            @if(isset($inProgress))
                <h3>Your cases in progress</h3>
                <table class="table table-responsive">
                    <tr>
                        <th>Claimant</th>
                        <th>Defendant</th>
                        <th>Status</th>
                    </tr>


                    @foreach($inProgress as $item)

                        <tr>
                            <td> {{$item->claimant}} </td>
                            <td> {{$item->defendant}} </td>
                            <td> {{$item->status}} </td>
                        </tr>
                    @endforeach

                </table>

            @endif

    </div>

@endsection