@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Dashboard</div>

                @if($role == "User")
				<div class="panel-body">
					Welcome back {{Auth::user()->name}}!  <br>
                    You have {{$total}}
                    previous applications. <br>
					And {{$pending}} are not completed
				</div>
                @endif

                @if($role == "Registrar")

                    <div class="panel-body">
                        Welcome back {{Auth::user()->name}}!  <br>
                        There are {{$total}}
                        total applications. <br>
                        And {{$total - $pending}} have not been assessed.
                    </div>
                @endif

                @if($role == "Cashier")

                    <div class="panel-body">
                        Welcome back {{Auth::user()->name}}!  <br>
                        There are {{$total}}
                        total applications. <br>
                        @if($pending >0 && $pending == $total)
                            And they all need to be verified.
                        @endif
                        @if($pending > 0 && $pending != $total)
                            And {{$pending}} need to be verified
                        @endif
                        @if($pending <= 0)
                        And they all don't need verification.
                        @endif
                    </div>
                @endif
			</div>
		</div>
	</div>
</div>
@endsection
