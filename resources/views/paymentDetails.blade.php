@extends('app')

@section('content')

    <div class="container">
        @if(isset($status))
            <p class="panel panel-success">{{$status}}</p>
        @endif
        @if(isset($list))
        <div>
        <h3>Payment Details</h3>

        <p>
            Claimant: {{$list->claimant}} <br>
            Defendant: {{$list->defendant}}<br>

                Amount: {{$list->amount}} <br>
                Payment Status: {{$list->status}} <br>
            @if($list->status == "Paid")
                Verified by: {{$verified->name}}
                @endif

        </p>

        <h3>Attached Documents</h3>
        <ul class="list-group">
            @foreach($attachments as $item)
                <li class="list-group-item col-md-3"> <a href="{{$item->document}}">{{$item->name}}</a> </li>
            @endforeach
        </ul>
        </div>

        @if($list->status == "Pending")
            <form method="post" enctype="multipart/form-data" action="{{url('/payments/verify/'.$list->id)}}" class="form-group col-md-5">
                <input type="hidden" name="id" value="{{$list->id}}">
                <input type="hidden" name="appid" value="{{$list->appid}}">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="uid" value="{{Auth::user()->id}}">
                <button type="submit" class="btn btn-success">Verify</button>
            </form>
        @endif

        @endif


    </div>
@endsection