@extends('app')

@section('content')

    @if(isset($status))
        <p class="panel panel-success">{{$status}}</p>
    @endif


        {{--form for posting application--}}
    <form method="post" action="apply"  class="form-horizontal" id="form" enctype="multipart/form-data" >
        <input type="hidden" name="_token" value="{{csrf_token()}}">

        <div class="form-group">
            <label class="col-md-4 control-label">Claimant</label>
            <div class="col-md-6">
                <input type="text" name="claimant" class="form-control" value="{{old('claimant')}}">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label">Defendant</label>
            <div class="col-md-6">
                <input type="text" name="defendant" class="form-control" value="{{ old('defendant') }}">
            </div>
        </div>



    </form>
    <button onclick="add()" id="addDocument" class="btn btn-secondary">Add Document</button>
    <button onclick="done()" id="done" class="btn btn-secondary">Done</button>
        <script>
            function add(){
                var form = $('#form').append("<input type ='file' class='file'>");
                var num = 1;
                $('.file').each(function(){
                    this.setAttribute('name','file'+ num);
                    num++;
                });
            }

            function done(){
                var number = 0;
                $('.file').each(function (){ number++; });
                var form = $('#form');
                form.append("<input type='hidden' name='number' value='" + number + "' >");
                form.append("<button id='submit' type='submit' style='display:none' class='btn btn-primary'>Apply</button>")
                $('#submit').show();
                $('#addDocument').hide();
                $('#done').hide();
            }
        </script>



    </div>
    @endsection