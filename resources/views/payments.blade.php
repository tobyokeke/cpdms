@extends('app')

@section('content')
   <div class="container">
       @if(isset($list))
           <h3>Unverified payments</h3>
           <table class="table table-responsive">
               <tr>
                   <th>Claimant</th>
                   <th>Defendant</th>
                   <th>Amount</th>
                   <th></th>
               </tr>


               @foreach($list as $item)

                   <tr>
                       <td> {{$item->claimant}} </td>
                       <td> {{$item->defendant}} </td>
                       <td> {{$item->amount}} </td>
                       <td><a href="{{url('payments/verify/'.$item->id)}}"> <button class="btn btn-primary">Verify </button></a></td>
                   </tr>
               @endforeach

           </table>

       @endif

   </div>
@endsection