<?php namespace CDMS;

use Illuminate\Database\Eloquent\Model;

class Application extends Model {

	protected $table = 'applications';
    protected $fillable = ['suitno','judge'];
}



class Comment extends Model {

    protected $table = 'comments';

}



class Applications_Documents extends Model {
    public $timestamps = false;
    protected $table = 'applications_documents';
}

class Applications_Comments extends Model {
    public $timestamps = false;
    protected $table = 'applications_comments';
}

