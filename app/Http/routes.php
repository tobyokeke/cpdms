<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


//Route::get('/', 'WelcomeController@index');
Route::get('/', 'HomeController@index');
Route::get('/apply','HomeController@apply');
Route::get('/viewApplications','HomeController@viewApplications');
Route::get('/assessApplications','HomeController@assessApplications');
Route::get('/appDetails/{id}','HomeController@assessDetails');
Route::get('/assignCase/{id}','HomeController@assignCase');
Route::get('/pay/{id}', 'HomeController@pay');
Route::get('/payments/verify','HomeController@verifyPayment');
Route::get('payments/verify/{id}','HomeController@approvePaymentDocs');


Route::post('payments/verify/{id}','HomeController@approvePaymentDocsPost');
Route::post('/apply','HomeController@applyPost');
Route::post('/appDetails/','HomeController@assessPost');
Route::post('/appDetails/update','HomeController@assessPostUpdate');
Route::post('/appDetails/assign/suit','HomeController@assessPostSuite');
Route::post('/pay','HomeController@payPost');
Route::post('/pay','HomeController@payPost');



Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('users/{age}','userController@index');

