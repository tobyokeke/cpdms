<?php namespace CDMS\Http\Middleware;

use Closure;

class birth {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

        if( $request->route()->getParameter('age') < 21)return 'sorry your underage';
        return $next($request);

	}

}
