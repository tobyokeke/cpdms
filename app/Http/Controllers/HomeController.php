<?php namespace CDMS\Http\Controllers;

use CDMS\Application;
use CDMS\Applications_Documents;
use CDMS\Document;
use CDMS\Payment;
use CDMS\Payments_Documents;
use CDMS\User;
use CDMS\Users_Applications;
use CDMS\Http\Requests\registerRequest;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
        $role = Auth::user()->role;
        if( $role == 'User') {

            $totalApplications = Users_Applications::where('users_id', Auth::user()->id)->get();
            $total = $totalApplications->count('applications_id');
            $appIds = $this->makeAnArray($totalApplications,'applications_id');

            $pendingApplications = Application::hydrateRaw("select * from APPLICATIONS where id in" . $appIds . " and status != 'Completed'")->count('id');

        }


        if($role == 'Registrar') {

            $totalApplications = Application::all();
            $total = $totalApplications->count('id');

            $pendingApplications = Application::hydrateRaw("select * from APPLICATIONS where status != 'Awaiting Assessment'")->count('id');


        }

        if($role == 'Cashier') {

            $totalApplications = Application::all();
            $total = $totalApplications->count('id');

            $pendingApplications = Application::where("status",'Awaiting Payment Verification')->count('id');


        }

        return view('home',['total'=>$total, 'pending'=>$pendingApplications, 'role'=> $role]);

	}

    public function apply(){
        return view('apply');
    }

    public function viewApplications(){

        $applicationsByUser = Users_Applications::hydrateRaw("select DISTINCT applications_id  as id from users_applications
        where users_id =" . Auth::user()->id);
        $appIds = $this->makeAnArray($applicationsByUser,'id');

        $applications = Application::hydrateRaw("select * from APPLICATIONS where status = 'Awaiting Assessment' and id in ".$appIds );
        $assessedApps = Application::hydrateRaw("select a.*, p.amount as amount from APPLICATIONS a inner join payments
 p on a.id = p.applications_id where a.status in ('Awaiting Payment','Awaiting Payment Verification') and a.id in ".$appIds);
        $appsInProgress = Application::hydrateRaw("select a.*, p.amount as amount from APPLICATIONS a inner join payments
 p on a.id = p.applications_id where a.status not in ('Awaiting Assessment','Awaiting Payment','Awaiting Payment Verification')
  and a.id in ".$appIds);
        return view('viewApplications',['list' => $applications, 'assessed' => $assessedApps, 'inProgress' => $appsInProgress]);
    }

    public function assessDetails($id){
        $app = Application::find($id);
        $attachments = Applications_Documents::where('applications_id',$id)->get();
        $attachments = $this->makeAnArray($attachments,'documents_id');

        $documents = Document::hydrateRaw("select * from documents where id in ".$attachments );
        $payment = Payment::where('applications_id',$id)->get()->first();
        return view('assessDetails',['list'=> $app , 'attachments' => $documents,'payment'=>$payment]);

    }

    public function assessPost (Request $request){
        $amount = $request->input('amount');
        $id = $request->input('id');

        $payments = new Payment();
        $payments->applications_id = $id;
        $payments->status = 'Pending';
        $payments->amount = $amount;
        $payments->users_id = Auth::user()->id;
        $payments->save();
        $payment = Payment::where('applications_id',$id)->get();
        Application::where('id',$id)->update(['status'=>'Awaiting Payment']);
        return redirect('/appDetails/'.$id);
    }

    public function assessPostUpdate(Request $request){
        $amount = $request->input('amount');
        $id = $request->input('id');

        $payment = Payment::where('applications_id',$id)->update(['amount' =>$amount]);
        Application::where('id',$id)->update(['status'=>'Awaiting Payment']);
        return redirect('appDetails/'.$id)->with('status','Successfully Updated');
    }

    public function assessPostSuite(Request $request){
        $id = $request->input('id');
        $suitno = $request->input('suitMotion');
        $judge = $request->input('judge');

        $application = Application::find($id);
        $application->update(['suitno' => $suitno, 'judge' => $judge]);


        return redirect('appDetails/'.$id)->with('status','Successfully Assigned Case');
    }

    public function assessApplications(){
        $pendingApps = Application::hydrateRaw("select * from APPLICATIONS where status = 'Awaiting Assessment' order by created_at desc");
        $assessedApps = Application::hydrateRaw("select a.*, p.amount as amount from APPLICATIONS a inner join payments p on a.id = p.applications_id where a.status = 'Awaiting Payment'");
        $attention = Application::hydrateRaw("select a.*, p.amount as amount from APPLICATIONS a inner join payments p on a.id = p.applications_id where a.status not in ('Awaiting Payment','Awaiting Payment Verification')  order by created_at desc");
        return view('assess',['list'=> $pendingApps, 'assessed' => $assessedApps,'attention' => $attention]);

    }


	public function applyPost(Request $request){


        $uid = Auth::user()->id;
        $application = new Application();
        $application->claimant = $request->input('claimant');
        $application->defendant = $request->input('defendant');
        $application->save();

        $userApplication = new Users_Applications();
        $userApplication->users_id = $uid;
        $userApplication->applications_id = $application->id;
        $userApplication->save();

        $numOfFiles = $request->input('number');
        if($numOfFiles > 0){
            for( $i = 1; $i < $numOfFiles + 1; $i++){
                $key = 'file'.$i;
                $filename = $request->file($key)->getClientOriginalName();

                $request->file($key)->move('userUploads/applications/',$filename);
                $document = new Document();
                $document->name = $filename;
                $document->document = 'http://localhost/legal/public/userUploads/applications/'. $filename;
                $document->save();

                $appDocs = new Applications_Documents();
                $appDocs->applications_id = $application->id;
                $appDocs->documents_id = $document->id;
                $appDocs->save();

            }
        }

        return view('apply',['status' => 'Application Created Successfully']);

    }

    public function pay($id){
        $app = Application::find($id);
        $pay = Payment::where('applications_id',$id)->get()->first();

        return View('pay',['application' => $app, 'pay' => $pay]);
    }

    public function payPost(Request $request){
        $numOfFiles = $request->input('number');
        $payId = $request->input('id');

        if($numOfFiles > 0){
            for( $i = 1; $i < $numOfFiles + 1; $i++){
                $key = 'file'.$i;
                $filename = $request->file($key)->getClientOriginalName();

                $request->file($key)->move('userUploads/paymentDocs',$filename);
                $document = new Document();
                $document->name = $filename;
                $document->document = 'http://localhost/legal/public/userUploads/paymentDocs/'. $filename;
                $document->save();

                $payDocs = new Payments_Documents();
                $payDocs->payments_id = $payId;
                $payDocs->documents_id = $document->id;
                $payDocs->save();


            }
        }
        $payment = Payment::find($payId);
        $application = Application::find($payment->applications_id);
        $application->status = 'Awaiting Payment Verification';
        $application->save();

        return view('pay',['status' => 'Payment documents posted Successfully']);
    }

    public function verifyPayment(){
        $payment = Application::hydrateRaw("SELECT a.claimant, a.defendant,p.id, p.amount FROM `payments` p inner join
            applications a on a.id = p.applications_id WHERE p.status = 'Pending' and a.status = 'Awaiting Payment Verification'");

       return view('payments',['list' => $payment]);
    }

    public function approvePaymentDocs($id){

        $payment = Application::hydrateRaw("SELECT a.claimant,a.id as appid , a.defendant,p.id,p.users_id, p.amount, p.status
            FROM `payments` p inner join applications a on a.id = p.applications_id WHERE
             p.id =".$id)->first();
        $attachmentsIds = Payments_Documents::where('payments_id',$id)->get();
        $attachmentsIdArray = $this->makeAnArray($attachmentsIds,'documents_id');
        $attachments = Document::hydrateRaw('select * from documents where id in '.$attachmentsIdArray);
        $verifyingUser = User::where('id',$payment->users_id)->get()->first();

       return view('paymentDetails',['list' => $payment,'attachments'=>$attachments,'verified'=> $verifyingUser]);

    }

    public function approvePaymentDocsPost(Request $request){
        $cashier = $request->input('uid');
        $payId = $request->input('id');
        $appId = $request->input('appid');

        Payment::where('id',$payId)->update(['status'=>'Paid', 'users_id' => $cashier]);
        Application::where('id',$appId)->update(['status' => 'Assigning Case']);

//        return redirect('/payments/verify/'.$payId);
        return view('paymentDetails',['status'=>'Payment Successfully Verified ']);
    }

    public function uploadFiles(Request $request){
        $numOfFiles = $request->input('number');
        for( $i = 0; $i< $numOfFiles; $i++){

            $filename = $request->file('file'.$i)->getClientOriginalName();

            $request->file('file'.$i)->move('userUploads/',$filename);

        }

        return 'done';
    }

    public function makeAnArray($data,$id){
        $appIds = "";

        foreach ($data as $item){
            $appIds .= $item->$id .',';
        }
        $appIds .= "0";
        $appIdsInBracket = "(".$appIds.")";
        return $appIdsInBracket;

    }

    public function removeSlashes($array,$property){
        foreach($array as $item) {
            echo stripslashes($item->$property);
        }
        return $array;
    }


}
