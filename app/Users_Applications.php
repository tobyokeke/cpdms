<?php namespace CDMS;

use Illuminate\Database\Eloquent\Model;

class Users_Applications extends Model {

    public $timestamps = false;
    protected $table = 'users_applications';

}
