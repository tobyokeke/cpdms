<?php namespace CDMS;

use Illuminate\Database\Eloquent\Model;

class Payments_Documents extends Model {

    public $timestamps = false;
    protected $table = 'payments_documents';

}
